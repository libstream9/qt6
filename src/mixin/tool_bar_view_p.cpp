#include <stream9/qt6/mixin/tool_bar_view_p.hpp>

#include <cassert>

#include <QModelIndex>
#include <QAction>

namespace stream9::qt6::mixin::tool_bar_view {

SlotsDelegate::
SlotsDelegate(Slots &host)
    : m_host { host }
{}

void SlotsDelegate::
onActionTriggered(QAction* const action)
{
    assert(action);
    m_host.onActionTriggered(*action);
}

} // namespace stream9::qt6::mixin::tool_bar_view

#include <QCoreApplication>
#include <QDebug>
#include <QEvent>
#include <QMouseEvent>
#include <QWidget>

namespace stream9::qt6::mixin::tool_bar_view {

MouseMoveEventCapturer::
MouseMoveEventCapturer(QWidget &target)
    : m_target { target }
{}

bool MouseMoveEventCapturer::
eventFilter(QObject* const, QEvent* const event)
{
    if (event->type() == QEvent::MouseMove) {
        auto* const ev = dynamic_cast<QMouseEvent*>(event);
        assert(ev);
        const auto &pos = m_target.mapFromGlobal(ev->globalPosition());
        const auto &rect = m_target.rect();
        if (rect.contains(pos.toPoint())) {
            QMouseEvent newEv {
                ev->type(), pos, ev->globalPosition(),
                ev->button(), ev->buttons(), ev->modifiers()
            };
            QCoreApplication::sendEvent(&m_target, &newEv);
        }
    }
    return false;
}

} // namespace stream9::qt6::mixin::tool_bar_view

#include <QCoreApplication>
#include <QEvent>
#include <QObject>
#include <QMouseEvent>

namespace stream9::qt6::mixin::tool_bar_view {

bool ToolBarMiddleButtonHandler::
eventFilter(QObject* const obj, QEvent* const ev)
{
    //qDebug() << __func__ << obj << ev;
    assert(obj);
    assert(ev);

    const auto type = ev->type();
    if (type != QEvent::MouseButtonPress &&
        type != QEvent::MouseButtonRelease)
    {
        return false;
    }

    auto* const event = dynamic_cast<QMouseEvent*>(ev);
    if (event->button() != Qt::MiddleButton) return false;

    // Translate middle click into left click
    QMouseEvent newEvent {
        event->type(), event->pos(), event->globalPosition(),
        Qt::LeftButton, Qt::LeftButton, event->modifiers()
    };
    QCoreApplication::sendEvent(obj, &newEvent);

    return false;
}

} // namespace stream9::qt6::mixin::tool_bar_view
