#include <stream9/qt6/mixin/menu_view_p.hpp>

#include <cassert>

#include <QAction>
#include <QModelIndex>

namespace stream9::qt6::mixin::menu_view {

SlotsDelegate::
SlotsDelegate(Slots &host)
    : m_host { host }
{}

void SlotsDelegate::
onTriggered(QAction* const action)
{
    assert(action);
    m_host.onTriggered(*action);
}

void SlotsDelegate::
onHovered(QAction* const action)
{
    assert(action);
    m_host.onHovered(*action);
}

void SlotsDelegate::
onAboutToShow()
{
    m_host.onAboutToShow();
}

} // namespace stream9::qt6::mixin::menu_view
