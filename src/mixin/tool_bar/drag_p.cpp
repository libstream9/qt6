#include <stream9/qt6/mixin/tool_bar/drag_p.hpp>

#include <cassert>

#include <QAction>
#include <QCoreApplication>
#include <QDebug>
#include <QEvent>
#include <QMouseEvent>
#include <QWidget>

namespace stream9::qt6::mixin::tool_bar::drag {

MouseEventCapturer::
MouseEventCapturer(QWidget &destination)
    : m_destination { destination }
{}

bool MouseEventCapturer::
eventFilter(QObject* const, QEvent* const event)
{
    assert(event);

    const auto type = event->type();
    if (type == QEvent::MouseMove || isLeftButtonPress(event)) {
        auto ev = translate(event);
        QCoreApplication::sendEvent(&m_destination, &ev);
    }

    return false;
}

bool MouseEventCapturer::
isLeftButtonPress(QEvent* const ev) const
{
    assert(ev);
    if (ev->type() == QEvent::MouseButtonPress) {
        auto* const event = dynamic_cast<QMouseEvent*>(ev);
        if (event && event->button() == Qt::LeftButton) {
            return true;
        }
    }
    return false;
}

QMouseEvent MouseEventCapturer::
translate(QEvent* const ev) const
{
    auto* const event = dynamic_cast<QMouseEvent*>(ev);
    assert(event);

    const auto &pos = m_destination.mapFromGlobal(event->globalPosition());
    return QMouseEvent {
        event->type(), pos, event->globalPosition(),
        event->button(), event->buttons(), event->modifiers()
    };
}

} // namespace stream9::qt6::mixin::tool_bar::drag
