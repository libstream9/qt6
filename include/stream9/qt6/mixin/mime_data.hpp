#ifndef STREAM9_QT6_MIXIN_MIME_TYPE_HPP
#define STREAM9_QT6_MIXIN_MIME_TYPE_HPP

#include <QByteArray>
#include <QMimeData>
#include <QAction>

namespace stream9::qt6::mixin {

inline void
setAction(QMimeData &data, const QAction &action)
{
    const auto &bytes = QByteArray::number(reinterpret_cast<qulonglong>(&action));
    data.setData("application/x-action-id", bytes);
}

inline QAction *
getAction(const QMimeData &data)
{
    const auto &bytes = data.data("application/x-action-id");
    auto* const action = reinterpret_cast<QAction*>(bytes.toULongLong());

    return action;
}

} // namespace stream9::qt6::mixin

#endif // STREAM9_QT6_MIXIN_MIME_TYPE_HPP
