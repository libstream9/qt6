#ifndef STREAM9_QT6_MIXIN_TOOLBAR_DRAG_P_HPP
#define STREAM9_QT6_MIXIN_TOOLBAR_DRAG_P_HPP

#include <QObject>

class QAction;
class QMouseEvent;
class QWidget;

namespace stream9::qt6::mixin::tool_bar::drag {

class MouseEventCapturer : public QObject
{
public:
    MouseEventCapturer(QWidget &destination);

    bool eventFilter(QObject* const, QEvent* const);

private:
    bool isLeftButtonPress(QEvent* const) const;
    QMouseEvent translate(QEvent* const) const;

private:
    QWidget &m_destination;
};

} // namespace stream9::qt6::mixin::tool_bar::drag

#endif // STREAM9_QT6_MIXIN_TOOLBAR_DRAG_P_HPP
