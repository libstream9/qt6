#ifndef STREAM9_QT6_MIXIN_MENU_VIEW_DRAG_HPP
#define STREAM9_QT6_MIXIN_MENU_VIEW_DRAG_HPP

#include "../action_view/drag.hpp"
#include "../menu/drag.hpp"

namespace stream9::qt6::mixin::menu_view {

template<typename BaseT>
using Drag = mixin::action_view::Drag<
             mixin::menu::Drag<BaseT>>;

} // namespace stream9::qt6::mixin::menu_view

#endif // MIXIN_MENU_VIEW_DRAG_HPP
