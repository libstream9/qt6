#ifndef STREAM9_QT6_MIXIN_TOOL_BAR_VIEW_DRAG_HPP
#define STREAM9_QT6_MIXIN_TOOL_BAR_VIEW_DRAG_HPP

#include "../action_view/drag.hpp"
#include "../tool_bar/drag.hpp"

namespace stream9::qt6::mixin::tool_bar_view {

template<typename BaseT>
using Drag = mixin::action_view::Drag<
             mixin::tool_bar::Drag<BaseT>>;

} // namespace stream9::qt6::mixin::tool_bar_view

#endif // STREAM9_QT6_MIXIN_TOOL_BAR_VIEW_DRAG_HPP
