#ifndef STREAM9_QT6_MIXIN_MENU_TOOL_TIP_HPP
#define STREAM9_QT6_MIXIN_MENU_TOOL_TIP_HPP

#include <QString>

class QEvent;
class QWidget;

namespace stream9::qt6::mixin::menu {

template<typename BaseT>
class ToolTip : public BaseT
{
    using Base = BaseT;
public:
    ToolTip(QWidget* const parent);

protected:
    // @override QObject
    bool event(QEvent*) override;
};

} // namespace stream9::qt6::mixin::menu

#include <cassert>

#include <QDebug>
#include <QEvent>
#include <QHelpEvent>
#include <QToolTip>
#include <QWidget>

namespace stream9::qt6::mixin::menu {

template<typename BaseT>
ToolTip<BaseT>::
ToolTip(QWidget* const parent)
    : Base { parent }
{}

template<typename BaseT>
inline bool ToolTip<BaseT>::
event(QEvent* const ev)
{
    assert(ev);
    const auto result = Base::event(ev);

    [this, &ev] {
        if (ev->type() == QEvent::ToolTip) {
            auto* const event = dynamic_cast<QHelpEvent*>(ev);
            auto* const action = this->actionAt(event->pos());
            if (!action) return;

            const auto &toolTip = action->toolTip();
            if (toolTip.isEmpty()) return;
            QToolTip::showText(event->globalPos(), toolTip);
        }
    }();

    return result;
}

} // namespace stream9::qt6::mixin::menu

#endif // STREAM9_QT6_MIXIN_MENU_TOOL_TIP_HPP
